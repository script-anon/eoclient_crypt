
LIBRARY_NAME=eoclient_crypt
LIBRARY_FILE=lib$(LIBRARY_NAME).so
HEADER_FILE = eocrypt.h

TARGET=release

# Set installation directory
ifeq ($(PREFIX),)
    PREFIX := /usr/local
endif

# Release
ifeq ($(filter release,$(MAKECMDGOALS)),release)
TARGET = release
endif

# Debug
ifeq ($(filter debug,$(MAKECMDGOALS)),debug)
TARGET = debug
endif

# Output directory
TARGET_DIR=target/$(TARGET)

# Library output directory
LIBRARY_DIR = $(TARGET_DIR)/$(LIBRARY_FILE)

# Rules

.PHONY: install

# Install the library file and header
install:
	install $(LIBRARY_DIR) $(DESTDIR)$(PREFIX)/lib/$(LIBRARY_FILE)
	install $(HEADER_FILE) $(DESTDIR)$(PREFIX)/include/$(HEADER_FILE)

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/lib/$(LIBRARY_FILE)
	rm -f $(DESTDIR)$(PREFIX)/include/$(HEADER_FILE)
