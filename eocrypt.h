#ifndef OUTPOST_CRYPT_BINDINGS_H
#define OUTPOST_CRYPT_BINDINGS_H

#ifdef __cplusplus
extern "C" {
#endif

struct OccProtocol;

OccProtocol* occ_protocol_new(uint8_t initiator);
void occ_protocol_delete(OccProtocol* p);

#define OCC_EVENT_NONE                  0
#define OCC_EVENT_RECV                  1
#define OCC_EVENT_OUTGOING              2
#define OCC_EVENT_HANDSHAKE_FINISHED    3
#define OCC_EVENT_ERROR                 4

#define OCC_CHANNEL_BINDING_TOKEN_LEN   32

uint8_t occ_protocol_next_event(OccProtocol* p);
uint8_t* occ_protocol_recv_data(OccProtocol* p, size_t* len_out);
uint8_t* occ_protocol_outgoing_data(OccProtocol* p, size_t* len_out);
uint8_t* occ_protocol_handshake_finished_channel_binding_token(OccProtocol* p);
uint8_t* occ_protocol_error_message(OccProtocol* p, size_t* len_out);

void occ_protocol_open(OccProtocol* p);
void occ_protocol_send(OccProtocol* p, uint8_t* msg, size_t len);
void occ_protocol_incoming(OccProtocol* p, uint8_t* msg, size_t len);

#ifdef __cplusplus
}
#endif

#endif // OUTPOST_CRYPT_BINDINGS_H
