# Everfree Outpost Client Crypt Library

This is the library used for Everfree Outpost and for the Everfree Outpost CLI chat client.

# Building
To build this library run `cargo build --release`.

To install this library run `sudo make install`.

